#include <iostream>
#include <vector>
#include <stack>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <fstream>
#include "vertex.h"

class formula
{
    private:
        int variables;
        std::vector<std::pair<int,int> > f;
        std::vector<std::vector<int> > tarjan(
                std::vector<std::vector<int> > matrix);
        void strongconnect(int v,
                std::vector<std::vector<int> >&m,
                std::vector<std::vector<int> >&components,
                std::vector<vertex> &vertexes,
                std::stack<int> &S,
                std::vector<int> &inS,
                int &index);


    public:
        formula(std::vector<std::pair<int,int> > f);
        std::vector<std::pair<int,int> > SAT();

};
