#include <iostream>
#include <string>

#include <sstream>
#include <fstream>

#include <vector>

#include <cassert>
#include <cstdlib>

#include "formula.h"

using namespace std;

void help() 
{
    cerr << "./az [file.in] [file.out]" << endl;
}

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        cerr << "Sorry, unproper use" << endl;
        help();
        return 1;
    }

    ifstream fIn(argv[1]);

    string line;
    vector<pair<int,int> > pairs;

    if (!fIn.is_open())
    {
        cerr << "Unable to open given file file" 
            << argv[1]
            << ", trying default file.in" 
            << endl;
        fIn.open("file.in");

        if (!fIn.is_open())
        {
            cerr << "Open failed" << endl;
            return 2;
        }
    }

    while (getline(fIn, line))
    {
        // linia to jeden nawias

        string sVar1;
        string sVar2;

        cout << "Line: " << line << endl;

        stringstream ss(line);
        
        if (!getline(ss,sVar1, ','))
        {
            cerr << "Sth wrong" << endl;
            exit(1);
        }

        if (!getline(ss,sVar2, ','))
        {
            cerr << "Sth wrong" << endl;
            exit(1);
        }

        pair<int,int> p;

        p.first = atoi(sVar1.c_str());
        p.second = atoi(sVar2.c_str());

        int varId1 = abs(p.first)-1;
        int negId1 = (p.first < 0) ? 1 : 0;
        int varId2 = abs(p.second)-1;
        int negId2 = (p.second < 0) ? 1 : 0;
        p.first = 2*varId1 + negId1;
        p.second = 2*varId2 + negId2;


        pairs.push_back(p);

        cout << "Var1: " << p.first << ", Var2: " << p.second << endl;
        
    }

    fIn.close();

    formula f(pairs);
    f.SAT();

    return 0;
}
