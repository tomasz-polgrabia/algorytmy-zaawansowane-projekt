#include "formula.h"
#include <sstream>

using namespace std;

formula::formula(vector<pair<int,int> > f)
{
    // TODO
    this->f = f;
    variables = 0;

    for (auto p : f)
    {
        variables = max(abs(p.first), variables);
        variables = max(abs(p.second), variables);
    }
    variables++;
    variables /= 2;
}

vector<pair<int,int> > formula::SAT()
{
    int n = variables;
    vector<vector<int> >matrix(2*n);
    for (int i = 0; i < 2*n; i++)
        matrix[i].resize(2*n);

    // our graph G(F) 
    // F - our formula
    // |G(F)| = 2*n
    // n - count of variables


    vector<pair<int,int> >v;

    for (auto &p: f)
    {
        cout << "TODO " 
             << "First: " << p.first
             << ", second: " << p.second
             << endl;
    }

    for (auto row : matrix)
    {
        row.clear();
        row.resize(2*n);
        for (int i = 0; i < 2*n; i++)
            row[i] = 0;
    }

    for (auto &p : f)
    {
        int first = p.first;
        int second = p.second;
        // FIXME
        cerr << "Linia: " << first << " -> " << second << endl;
        int oFirst = (first % 2 == 0) ? first + 1 : first - 1;
        int oSecond = (second % 2 == 0) ? second + 1 : second - 1;
    
        cerr << "u' -> v " << oFirst << " -> " << second << endl;
        cerr << "v' -> u " << oSecond << " -> " << first << endl;

        matrix.at(oFirst).at(second) = 1; // u' -> v 
        matrix.at(oSecond).at(first) = 1; // v' -> u
    }

    vector<vector<int> >components =
        tarjan(matrix);

    cerr << "Komponentów z tarjan " << components.size() << endl;

    vector<int> idComponent(matrix.size());
    int nMatrix = matrix.size();
    int nComponents = components.size();

    for (int i = 0; i < nMatrix; i++)
        idComponent[i] = -1;

    for (int row = 0; row < components.size(); row++)
        for (int i = 0; i < components[row].size(); i++)
        {
            int id = components[row][i];
            idComponent[id] = row;
        }

    for (int i =0; i < nMatrix; i++)
        assert(idComponent[i] >= 0 && idComponent[i] < nComponents);

    vector<int>pairs(nComponents);
    for (int &el : pairs)
        el = -1;

    cerr << "Komponentów jest: " << nComponents << endl;

    for (int i = 0; i < nMatrix/2; i++)
    {
        int id1 = idComponent[2*i];
        int id2 = idComponent[2*i+1];
        cerr << "El " << 2*i << " belongs to: " << id1
            << ", el: " << 2*i+1 << " belongs to: " << id2
            << endl;
        if (id1 == id2)
        {
            cerr << "Not able to satisfy" << endl;
            return v;
        }
        if (pairs[id1] < 0)
            pairs[id1] = id2;
        if (pairs[id2] < 0)
            pairs[id2] = id1;
    }

    for (int i = 0; i < nComponents; i++)
        assert(pairs[i] >= 0 && pairs[i] < nComponents);

    vector<bool> markedComponents(components.size());
    vector<int> nodes(matrix.size());

    for (int i = 0; i < nComponents ; i++)
    {
        cerr << "Component " << i << ". is to be marked" << endl;
        if (markedComponents[i])
        {
            cerr << "Marked component " << i << "., omitting..." << endl;
            continue;
        }

        int idOpponent = pairs[i];
        assert(idOpponent >= 0 && idOpponent < nComponents);
        if (idOpponent == i)
        {
            cout << "Przerwa, nie wszystko" << endl;
            return v;
        }

        cerr << "Marking component " << i << " as true" << endl;
        cerr << "Marking component " << idOpponent << " as false" << endl;


        for (int &el : components[i])
        {
            nodes[el] = 1;
            cerr << "Element " << el << " marked as true" << endl;
        }

        for (int &el : components[idOpponent])
        {
            nodes[el] = 0;
            cerr << "Element " << el << " marked as false" << endl;
        }

        markedComponents.at(i) = true;
        markedComponents.at(idOpponent) = true;

    }

    for (int i = 0; i < nodes.size()/2; i++)
    {
        assert (nodes[2*i] >= 0 && nodes[2*i] <= 1);
        assert (nodes[2*i+1] >= 0 && nodes[2*i+1] <= 1);
        assert (nodes[2*i] + nodes[2*i+1] == 1);

    }

    for (int i =0; i < nodes.size(); i++)
        if (nodes[i])
            cerr << i << ",";

    cerr << endl;


    for (int i =0; i < nodes.size(); i++)
        if (nodes[i])
        {
            cerr << i/2+1;
            if (i % 2 == 1)
                cerr << "'";
            cerr << ",";
        }

    cerr << endl;

    ofstream oS("graph.dot"); 
    oS << "digraph G {" << endl;

    int nVertices = matrix.size();

    for (int i = 0; i < nVertices; i++)
    {
        for (int j = 0; j < nVertices; j++)
        {
            if (matrix[i][j] < 1)
                continue;

            /*
            char varA = (char)('a'+i/2);
            char varB = (char)('a'+j/2);

            string suffixA = "";
            if (i % 2 == 1)
                suffixA = "n";

            string suffixB = "";
            if (j % 2 == 1)
                suffixB = "n";


            oS << varA << suffixA << " -> " << varB << suffixB << ";" << endl;

            */


            oS << i << " -> " << j << ";" << endl;

        }

    }

    for (int i = 0; i < nMatrix; i++)
    {
        string color = (nodes[i]) ? "green" : "red";
        char varName = (char)('a'+i/2);
        stringstream negation;
        if (i % 2 == 1)
            negation << "'";

        oS << i << " [ " 
             << "color=" << color << ", "
             << "label=\"" << varName << negation.str() << "\\n (component: " << idComponent[i] << ")" << "\""
             << " ];" << endl;
    }

    oS << "}";
    oS.close();


    return v;
}

vector<vector<int> > formula::tarjan(vector<vector<int> > matrix)
{
    int n = matrix.size();
    vector<vector<int> >components;
    for (auto &row : components)
        row.resize(n);
    vector<vertex> vertexes(n);
    vector<int> inS(n);

    int index = 0;
    stack <int> S;

    for (int i = 0; i < n; i++)
    {
        cerr << "Tarjan loop i: " << i <<
            ", liczba komponentow: " << components.size() << endl;
        if (vertexes.at(i).index < 0)
        {
            strongconnect(i, matrix,components,vertexes, S, inS,index);
        }
            cerr << "Index " << i << ", has topological index: " << vertexes.at(i).index << endl;
    }

    return components;
}

void formula::strongconnect(int v,
        vector<vector<int> >&m,
        vector<vector<int> >&components,
        vector<vertex> &vertexes,
        stack<int> &S,
        vector<int> &inS,
        int &index)
{
    cerr << "Strongconnect for " << v << endl;
    int n = m.size();
    vertexes[v].index = index;
    vertexes[v].lowlink = index;
    index++;
    S.push(v);
    inS[v] = 1;

        for (int w = 0; w < n; w++)
            if (m.at(v).at(w))
            {
                cerr << "StrongConnect, Krawedz: (" << v << " - " << w << ")" << endl;
                if (vertexes.at(w).index < 0)
                {
                    // w.index is undefined
                    /*
                    strongconnect(w,m,components,
                            vertexes,
                            S,inS,index); */
                    strongconnect(w,
                            m,
                            components,
                            vertexes,
                            S,
                            inS,
                            index);
                    vertexes[v].lowlink = min(
                            vertexes[v].lowlink,
                            vertexes[w].lowlink);
                }
                else if (inS[w]) // FIXME dodaj dodawanie usuwanie 
                                 // zawierania się w stosie
                {
                    // S contains w
                    vertexes[v].lowlink = min(
                            vertexes[v].lowlink,
                            vertexes[w].index);
                }
            }

    // If v is a root node, pop the stack and generate an SCC
    
    if (vertexes[v].lowlink == vertexes[v].index)
    {
        // start a new strongly connected component
        vector<int> component;

        int w = -1;

        do 
        {

            w = S.top();
            S.pop();
            inS[w] = 0;

            component.push_back(w);
            // add w to current strongly connected component 

        } while (w != v);

        cerr << "Dodaje komponent: (";
        int nX = component.size();
        for (int i = 0; i < nX-1; i++)
            cerr << component[i] << ",";
        cerr << component[nX-1] << ")" << endl;


        components.push_back(component);
        // output the current strongly connected component 
    }

}
