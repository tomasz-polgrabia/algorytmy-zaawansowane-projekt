SOURCES=$(wildcard *.cpp)
BINARIES=$(SOURCES:.cpp=.cpp.o)
CFLAGS=-std=c++11 -Wc++11-extensions -Wall -g

all: az

az: $(BINARIES)
	clang++ $(CFLAGS) -o $@ $^

%.cpp.o: %.cpp
	clang++ -c $(CFLAGS) -o $@ $<

.PHONY: clean graph

clean: 
	rm -f *.o *.dot *.png az

graph:
	dot -Tpng graph.dot -ograph.png
